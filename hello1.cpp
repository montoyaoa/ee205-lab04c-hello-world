/////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// @file hello1.cpp
// @version 1.0
// 
// Implements Hello World in C++
//
// @author Osiel Montoya <montoyao@hawaii.edu>
// @date   02_FEB_2021
//////////////////////////////////////////////////////////////////////////////

using namespace std;
#include <iostream>

int main(){

   cout << "Hello World";
   cout << endl;

}
